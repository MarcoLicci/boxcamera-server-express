﻿'use strict';
var express = require('express');
var router = express.Router();

var httpProxy = require('http-proxy'),
    proxy = httpProxy.createProxyServer({});

var redis = require("redis"),
    redisClient = redis.createClient({ host: "192.168.1.100" });

router.all('/*', function (req, res, next) {
    var camera = req.hostname.split('.')[0];
    redisClient.get(camera, function (err, reply) {
        console.log(reply);
        console.log(err)
        if (!err && reply) {
            proxy.web(req, res, {
                target: 'http://' + reply + ':8765/'
            });
        } else {
            next();
        }
    });
});

module.exports = router;
