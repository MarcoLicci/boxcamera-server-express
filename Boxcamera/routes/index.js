﻿'use strict';
var express = require('express');
var router = express.Router();

var subdomain = require('express-subdomain');
var camere = require('./camere.js');

router.use(subdomain('*.camere', camere));

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

module.exports = router;
